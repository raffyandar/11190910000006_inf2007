/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.keempat;

/**
 *
 * @author hp
 */
import java.util.Scanner;

public class HurufVokal {

    public static void main(String[] args) {
        char K;
        Scanner input = new Scanner(System.in);
        K = input.next().charAt(0);

        if (K == 'a' || K == 'i' || K == 'u' || K == 'e' || K == 'o') {
            System.out.println("Huruf Vokal");
        }

    }
}
