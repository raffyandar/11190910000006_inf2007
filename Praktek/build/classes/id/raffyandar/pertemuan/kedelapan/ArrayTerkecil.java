/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class ArrayTerkecil {
    int i, min = 9999;
    public int getTerkecil(int A[], int n){
        for (i = 0; i < A.length; i++) {
            if (A[i] < min) {
                min = A[i];
            }
        }
        min = min - 1;
        System.out.println("");
        System.out.println("elemen yang lebih kecil = " + min);
        
        return min;
    }
    
    public static void main(String[] args) {
        int i, n;
        Scanner in = new Scanner (System.in);
        ArrayTerkecil app = new ArrayTerkecil();
        
        System.out.print("N = ");
        n = in.nextInt();
        
        int[] A = new int[n];
        
        for (i = 0; i < A.length; i++) {
            System.out.print("Masukkan array [" + i + "]: ");
            A[i] = in.nextInt();
        }
        
        System.out.println("\nmatriks awal");
        for (i = 0; i < A.length; i++) {
            System.out.print(A[i] + "  ");
        }
        app.getTerkecil(A, n);
    }
}