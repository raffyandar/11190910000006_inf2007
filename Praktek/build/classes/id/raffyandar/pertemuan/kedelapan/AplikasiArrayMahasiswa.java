/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class AplikasiArrayMahasiswa {
    public static void main(String[] args) {
        int jumlahData;
        int nim;
        String nama;
        double nilai;
        
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan Jumlah Array = ");
        jumlahData=in.nextInt();
        
        Mahasiswa[] mahasiswas = new Mahasiswa[jumlahData];
        for (int i = 0; i <= jumlahData - 1; i++){
            System.out.println("Mahasiswa ke : " + (i + 1));
            System.out.print("Masukkan NIM = ");
            nim = in.nextInt();
            System.out.print("Masukkan Nama = ");
            nama = in.next();
            System.out.print("Masukkan Nilai = ");
            nilai = in.nextDouble();
            
            mahasiswas[i] = new Mahasiswa (nim, nama, nilai);
        }
        System.out.println("");
        System.out.println("Data Mahasiswa Pada Array");
        for (Mahasiswa mahasiswa : mahasiswas){
            System.out.println("NIM : " + mahasiswa.getNim() + "\t Nama : " + mahasiswa.getNama() + "\tNilai : " + 
                    mahasiswa.getNilai());
        }
    }
}
