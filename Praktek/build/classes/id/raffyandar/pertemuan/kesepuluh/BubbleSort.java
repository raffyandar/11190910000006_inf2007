package id.raffyandar.pertemuan.kesepuluh;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author hp
 */
public class BubbleSort {

    public int[] getBubbleSort(int L[], int n) {
        int i, k, temp;

        for (i = 0; i < n - 1; i++) {
            for (k = n -1 ; k > i; k--) {
                if (L[k] < L[k - 1]) {
                    temp = L[k];
                    L[k] = L[k - 1];
                    L[k - 1] = temp;
                    
                }
                System.out.println("i = " + i + ",k = " + (k - 1) + "-->" + L[k - 1]);
            }
        }
        return L;
    }
    
    
    public static void main(String[] args) {
        BubbleSort app = new BubbleSort();
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = L.length;
        int i;
        System.out.println("Sebelum Urut");
        System.out.println(Arrays.toString(L));
        int[] bubbleSort = app.getBubbleSort(L, n);
        System.out.println("Sesudah Urut");
        System.out.println(Arrays.toString(bubbleSort));
        
    }
}
