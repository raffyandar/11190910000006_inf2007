/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author hp
 */
public class SelectionSort {
    public int [] getSelectionSortMaks(int L[], int n){
        int i, j ,imaks ,temp;
        
        for (i = n-1; i > 0 ; i--){
            imaks = 0;
            for (j = 1; j< i+1 ; j++) {
                if (L[j] > L[imaks]) {
                    imaks = j;
                }
            }
            temp = L[i];
            L[i] = L[imaks];
            L[imaks] = temp;
        }
        return L;
    }
    public int [] getSelectionSortMin(int L[], int n){
        int i, j, imin, temp;
        
        for (i = 0; i<n-1; i++){
            imin = i;
            for (j = i+1; j < n; j++){
                if (L[j] < L[imin]){
                    imin = j;
                }
            }
            temp = L[i];
            L[i] = L[imin];
            L[imin] = temp;
        }
        return L;
    }
    
    public static void main(String[] args) {
        SelectionSort app = new SelectionSort();
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = L.length;
        System.out.println("Sebelum Urut");
        System.out.println(Arrays.toString(L));
        //app.getSelectionSortMaks(L, n);
        app.getSelectionSortMin(L, n);
        System.out.println("Sesudah Urut");
        System.out.println(Arrays.toString(L));
    }
}
