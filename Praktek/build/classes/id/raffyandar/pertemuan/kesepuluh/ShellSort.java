/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author hp
 */
public class ShellSort {
    public int[] getShellSort(int L[], int n) {
        int step, start;
        int i, j, y;
        boolean ketemu;
        
        step = n-1;
        while (step > 1){
            step = (step/3)+1;
            for (start = 0; start<step; start++){
                i = start + step;
                while (i <= n-1){
                    y = L[i];
                    j = i-step;
                    ketemu = false;
                    
                    while ((j>=0) && (!ketemu)){
                        if (y < L[j]){
                            L[j+step] = L[j];
                            j = j-step;
                        }else {
                            ketemu = true;
                        }
                    }
                    L[j+step] = y;
                    i = i+step;
                }
            }
        }
        return L;
    }
    public static void main(String[] args) {
        int L[] = {81, 94, 11, 96, 12, 35, 17, 95, 28, 58, 41, 75, 15};
        int n = L.length;
        
        ShellSort app = new ShellSort();
        System.out.println("Sebelum Urut");
        System.out.println(Arrays.toString(L));
        app.getShellSort(L, n);
        System.out.println("Setelah Urut");
        System.out.println(Arrays.toString(L));
    }
}
