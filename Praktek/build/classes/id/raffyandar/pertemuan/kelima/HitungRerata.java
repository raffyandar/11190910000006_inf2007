package id.raffyandar.pertemuan.kelima;

/**
 *
 * @author hp
 */
import java.util.Scanner;

public class HitungRerata {

    public static void main(String[] args) {
        int i, jumlah;
        float rerata;

        jumlah = 0;
        i = 0;
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();

        while (x != -1) {
            i = i + 1;
            jumlah = jumlah + x;
            x = in.nextInt();
        }
        if (i != 0) {
            rerata = (float) jumlah / i;
            System.out.println(rerata);
        } else {
            System.out.println("Tidak Ada Nilai Ujian Yang Dimasukkan");
        }

    }

}
