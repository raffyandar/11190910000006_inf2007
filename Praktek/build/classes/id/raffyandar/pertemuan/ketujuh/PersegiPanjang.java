/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.ketujuh;

/**
 *
 * @author hp
 */
public class PersegiPanjang {

    private double panjang;
    private double lebar;

    public PersegiPanjang() {
        System.out.println("Konstruktor Persegi Panjang");
    }

    public PersegiPanjang(double panjang, double lebar) {
        this.panjang = panjang;
        this.lebar = lebar;
    }

    public double getLuas() {
        return panjang * lebar;
    }

    public double getKeliling() {
        return 2 * (panjang + lebar);
    }

    public void getInfo() {
        System.out.println("Kelas Persegi Panjang");
    }
}
