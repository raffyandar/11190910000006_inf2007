/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class PencarianString {
    public int getStringSeqSearch(String L[], int n, String X){
        int i=0;
        while((i < n-1) && (!(L[i].equals(X)))){
            i = i + 1;
        }
        
        if (L[i].equals(X)){ 
            return i;
        }else{
            return -1;
    }
    }
    public int getStringBinSearch(String L[], int n, String X){
    int i = 0;
    int j = n - 1;
    int k = 0 ;
    boolean ketemu = false;
    
    while ((i <= j) && (!ketemu)) {
        k = ((i + j) / 2);
        if ((L[k]).equals(X)){
            ketemu = true;
        }else {
            if ((L[k].compareTo(X)) < 0){
                i = k + 1;
            }else {
                j = k - 1;
            }
        }
    }
    if (ketemu){
        return k;
    }else{
        return -1;
    }
    
}
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String A [] = {"a","i","u","e","o"};
        int n = 3;
        String X;
        System.out.print("Masukkan huruf vokal = ");
        X = in.next();
        PencarianString app = new PencarianString();
        System.out.println("Hasil Pencarian Beruntun");
        System.out.println("indeks ke = " + app.getStringSeqSearch(A, n, X));
        System.out.println("Hasil Pencarian BagiDua");
        System.out.println("indeks ke = " + app.getStringBinSearch(A, n, X));
    }
}
