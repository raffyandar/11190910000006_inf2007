/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class AnotherSearch {
    public int getAnotherSearch(int Array[], int x){
        int i;
        int indeks = -1;
        
        for (i=0; i<Array.length; i++){
            if (Array[i] == x){
                indeks = i;
            }
        }
        return indeks;
    }
    public static void main(String[] args) {
        int Array[] = {1,2,3,4,5,6,7,8};
        int x;
        int n = 8;
        
        AnotherSearch app = new AnotherSearch();
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan nilai x = ");
        x = in.nextInt();
        System.out.println("indeks = " + app.getAnotherSearch(Array, x));
    }
}
