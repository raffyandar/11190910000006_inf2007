/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class BinarySearch {
    private int i, j, k ;
    private boolean ketemu;
    public int getBinarySearch(int L[], int n, int x){
        
        i = 0;
        j = n-1;
        ketemu = false;
        
        while ((!ketemu) && (i <= j)){
            k = ((i + j)/2);
            if (L[k] == x){
                ketemu = true;
            }else {
                if (L[k] < x){
                    i = k + 1;
                }else {
                    j = k - 1;
                }
            }
        }
        if (ketemu){
            return k;
        }else {
            return -1;
        }
    }
    public static void main(String[] args) {
        int [] L = {13,14,15,16,21,76};
        int X, n = 6;
        
        BinarySearch app = new BinarySearch();
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan X = ");
        X = in.nextInt();
        
        System.out.println("index = " + app.getBinarySearch(L, n, X));
    }
}