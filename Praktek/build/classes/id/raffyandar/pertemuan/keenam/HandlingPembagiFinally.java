package id.raffyandar.pertemuan.keenam;

/**
 *
 * @author hp
 */
public class HandlingPembagiFinally {
    public static void main(String[] args) {
        try {
            int a = 10;
            int b = 0;
            int c = a/b;
            
            System.out.println("hasil : " + c);
        }catch (Throwable error) {
            System.out.println("ups, Terjadi error : ");
            System.out.println(error.getMessage());
        }finally {
           System.out.println("Pasti akan dijalankan");
        }
    }
}
    

