package id.raffyandar.pertemuan.keenam;

/**
 *
 * @author hp
 */
import java.util.Scanner;

public class HandlingThrow {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        try {
            System.out.println("Masukan Angka : ");
            int num = in.nextInt();
            if (num > 10) {
                throw new Exception();
            }
            System.out.println("Angka Kurang dari atau Sama Dengan 10");
        } catch (Exception err) {
            System.out.println("Angka lebih dari 10");
        }
        System.out.println("Selesai");
    }
}
