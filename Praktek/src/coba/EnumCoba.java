/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coba;

/**
 *
 * @author hp
 */
public class EnumCoba {

    public enum JenisKelamin {

        PRIA(1, "PRIA"), WANITA(2, "WANITA");

        private String strJenisKelamin;
        private int intJenisKelamin;

        private JenisKelamin(int intJenisKelamin, String strJenisKelamin) {
            this.intJenisKelamin = intJenisKelamin;
            this.strJenisKelamin = strJenisKelamin;
        }

        public String getStringValue() {
            return strJenisKelamin;
        }

        public int getintValuestatic() {
            return intJenisKelamin;
        }

    }
    public static void main(String[] args) {
         System.out.print(JenisKelamin.PRIA.intJenisKelamin);
         System.out.println(". " +JenisKelamin.PRIA.strJenisKelamin);
         System.out.print(JenisKelamin.WANITA.intJenisKelamin);
         System.out.println(". " +JenisKelamin.WANITA.strJenisKelamin);
    }
}
