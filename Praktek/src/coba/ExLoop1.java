/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coba;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class ExLoop1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int pil;
        
        do
        {   System.out.println("---------------------");
            System.out.println("    Menu Sederhana     ");
            System.out.println("---------------------");
            System.out.println("1.  Nama Diri  ");
            System.out.println("2.  Hobi Diri  ");
            System.out.println("3.  Makanan Favorit  ");
            System.out.println("4.  Keluar  ");
            System.out.println("Masukkan no Pilihan Anda (1-4)");
            pil = in.nextInt();
            switch(pil){
                case 1:
                    System.out.println("\nIni Menu No 1");
                    System.out.println("Nama Saya Budi \n");
                    break;
                case 2:
                    System.out.println("\nIni Menu No 2");
                    System.out.println("Hobi Budi Bermain Bola \n");
                    break;
                case 3:
                    System.out.println("\nIni Menu No 3");
                    System.out.println("Makanan Favorit Budi Adalah Soto Ayam");
                    break;
                case 4:
                    System.exit(0);
                    break;
            }
            
        }
        while (true);
    }
}
