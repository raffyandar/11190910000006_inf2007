/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coba;

import java.io.File;
import java.util.Calendar;

/**
 *
 * @author hp
 */
public class coba {
    public static void main(String[] args) throws Exception {
      fileInfo("/home/mano/Pictures/");
   }
   public static void fileInfo(String filePath) throws Exception {
      File file = new File(filePath);
      if (file.exists()) {
         if (file.isFile())
            properties(file);
         if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
               properties(f);
            }
         }

      } else {
         System.out.printf(
            "%s: ERROR: cannot open '%s' (No such file or directory)",
            filePath, filePath);
      }
   }

   private static void properties(File f) {
      Calendar c = Calendar.getInstance();
      c.setTimeInMillis(f.lastModified());
      String s = String.format("%1$tb %1$2te %1$tY %1$2tl:%1$2tM %1$Tp", c);
      System.out.printf("\n%s%s%s%s%s\t%8d\t%s\t%s", f.isDirectory() ? "d"
      : "-", f.canRead() ? "r" : "-", f.canWrite() ? "w" : "-", f
      .canExecute() ? "x" : "-", f.isHidden() ? "h" : "-",
      f.length(), s, f.getName());
   }
}

