/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class ArrayMaksimum {
        Scanner in = new Scanner(System.in);
        int Maks, i;
        int [] A = new int [100];
        
        public int ArrayMaksimum(int A[], int n){
            Maks = -9999;
             for (i=1; i<n; i++){
                if (A[i] > Maks) {
                Maks = A[i];
                }
             }
             return Maks;
        }
        
        public static void main(String[] args) {
            int i, n;
            Scanner in = new Scanner(System.in);
            
            ArrayMaksimum array = new ArrayMaksimum();
            System.out.print("Masukkan nilai N = ");
            n = in.nextInt();
            int A[] = new int[n];
            for (i=0; i<A.length; i++){
                A[i] = in.nextInt();
        }
        System.out.println("Elemen Terbesar = " + array.ArrayMaksimum(A,n));
    }
}
