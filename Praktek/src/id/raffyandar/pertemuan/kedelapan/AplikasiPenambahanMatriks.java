/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class AplikasiPenambahanMatriks {
    public static void main(String[] args) {

        int i, j, nBar, nKol;

        getPenambahanMatriks matriks = new getPenambahanMatriks();
        Scanner in = new Scanner(System.in);

        System.out.print("masukkan Baris: ");
        nBar = in.nextInt();
        System.out.print("Masukkan kolom: ");
        nKol = in.nextInt();

        int[][] A = new int[nBar][nKol];
        int[][] B = new int[nBar][nKol];
        int[][] C = new int[nBar][nKol];

        System.out.println("Input nilai Array A");
        for (i = 0; i < A.length; i++) {
            for (j = 0; j < A[i].length; j++) {
                System.out.print("Masukkan Array A [" + i + "," + j + "] : ");
                A[i][j] = in.nextInt();
            }
        }

        System.out.println("\nInput nilai Array B");
        for (i = 0; i < B.length; i++) {
            for (j = 0; j < B[i].length; j++) {
                System.out.print("Masukkan Array B [" + i + "," + j + "] : ");
                B[i][j] = in.nextInt();
            }
        }
        matriks.getPenambahanMatriks(A, B, nBar, nKol);
    }
}
