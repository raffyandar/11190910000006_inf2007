/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kedelapan;

/**
 *
 * @author hp
 */
public class getPenambahanMatriks {
    int j, i;
    
    public int[][] getPenambahanMatriks(int A[][], int B[][], int nBar, int nKol){
        int [][] C = new int[nBar][nKol];
        
        for (i=0; i<A.length; i++){
            for (j = 0; j < A[i].length; j++){
                C[i][j] = A[i][j] + B[i][j];
            }
        }
        System.out.println("");
        System.out.println("Hasil penjumlahan Array");
        for (i = 0; i < C.length; i++) {
            for (j = 0; j < C[i].length; j++) {
                System.out.print(C[i][j] + "  ");
            }
            System.out.println("");
        }
        return C;
    }        
}
