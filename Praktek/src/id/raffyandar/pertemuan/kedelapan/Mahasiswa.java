/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kedelapan;

/**
 *
 * @author hp
 */
public class Mahasiswa {

    private int nim;
    private String nama;
    private Double nilai;

    public Mahasiswa(int nim, String nama, Double nilai) {
        this.nim = nim;
        this.nama = nama;
        this.nilai = nilai;
    }

    public int getNim() {
        return nim;
    }

    public void setNim(int nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Double getNilai() {
        return nilai;
    }

    public void setNilai(Double nilai) {
        this.nilai = nilai;
    }
    

}
