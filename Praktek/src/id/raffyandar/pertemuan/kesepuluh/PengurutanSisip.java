/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kesepuluh;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author hp
 */
public class PengurutanSisip {
     public int[] getPengurutanSisip(int L[], int n){
        int i, y, j=0;
        boolean ketemu;
        
        for (i = 1; i >= n-1 ; i--){
            i = i-1;
            y = L[i];
            ketemu = false;
            
            while ((j >= 0) && (!ketemu)){
                if (y < L[j]){
                L[j+1] = L[j];
                j = j-1;
                }else {
                    ketemu = true;
                }
            }
            L[j+1] = y;
        }
        return L;
    }
     public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan Panjang Array = ");
        int n = in.nextInt();
        int L[] = new int[n];
        
        PengurutanSisip app = new PengurutanSisip();
        System.out.println("Sebelum Urut");
        for (int i = 0; i < L.length; i++) {
            System.out.print("Masukkan array [" + i + "]: ");
            L[i] = in.nextInt();
            app.getPengurutanSisip(L, n);
            System.out.println("Setelah Urut");
            System.out.println(Arrays.toString(L));
        }
    }
}
