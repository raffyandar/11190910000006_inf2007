/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kesepuluh;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author hp
 */
public class PengurutanApung {
    public int[] getPengurutanApung(int [] L, int n){
        int i, k, temp = 0;
        
        for (i=0 ; i<n-1 ; i++){
            for (k = n - 1; k > i; k--){
                if (L[k] > L[k - 1]) {
                    temp = L[k];
                    L[k] = L[k-1];
                    L[k-1] = temp;
                }
                
            }
        }
        return L;
    }
    public static void main(String[] args) {
        PengurutanApung app = new PengurutanApung();
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan Panjang Array = ");
        int n = in.nextInt();
        int L [] = new int[n];
        
        System.out.println("Sebelum di urutkan");
        for (int i = 0; i < L.length; i++) {
            System.out.print("Masukkan array [" + i + "]: ");
            L[i] = in.nextInt();
            int[] apung = app.getPengurutanApung(L, n);
        System.out.println("Setelah di Urutkan");
        System.out.println(Arrays.toString(apung));
        
        }
        //System.out.println(Arrays.toString(L) );
    }
}
