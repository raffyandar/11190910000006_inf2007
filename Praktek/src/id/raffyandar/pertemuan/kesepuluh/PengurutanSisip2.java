/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kesepuluh;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author hp
 */
public class PengurutanSisip2 {
    public int [] getPengurutanSisip2(int L[], int n){
        int i, j, y;
        
        for (i=0; i<n-1; i++){
            y = L[i];
            j = i;
            
            while ((j>0) && (y < L[j])){
                L[j] = L[j-1];
                j--;
            }
            L[j] = y;
        }
        
        return L;
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan Panjang Array = ");
        int n = in.nextInt();
        int L[] = new int [n];
        
        PengurutanSisip2 app = new PengurutanSisip2();
        System.out.println("Sebelum Urut");
        for (int k = 0; k < L.length; k++) {
            System.out.print("Masukkan array [" + k + "]: ");
            L[k] = in.nextInt();
            app.getPengurutanSisip2(L, n);
            System.out.println("Setelah Urut");
            System.out.println(Arrays.toString(L));
        }
    }
}
