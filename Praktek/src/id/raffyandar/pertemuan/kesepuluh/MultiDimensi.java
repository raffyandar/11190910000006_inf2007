/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kesepuluh;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author hp
 */
public class MultiDimensi {
int[][] getSortArray2Dimention(int[][] L, int[] L2, int Nbar, int Nkol) {
        int i, j, k = 0, m, n, imin, temp;
        for (i = 0; i < Nbar; i++) {
            for (j = 0; j < Nkol; j++) {
                L2[k] = L[i][j];
                k++;
            }
        }

        for (i = 0; i < ((Nbar * Nkol) - 1); i++) {
            imin = i;

            for (j = i + 1; j < (Nbar * Nkol); j++) {
                if (L2[j] < L2[imin]) {
                    imin = j;
                }
            }
            temp = L2[i];
            L2[i] = L2[imin];
            L2[imin] = temp;
        }

        k = 0;
        for (i = 0; i < Nbar; i++) {
            for (j = 0; j < Nkol; j++) {
                L[i][j] = L2[k];
                k++;
            }
        }
        return L;
    }

    public static void main(String[] args) {
        int i, j;
        int Nbar = 0, Nkol = 0;
        Scanner in = new Scanner(System.in);
        MultiDimensi app = new MultiDimensi();

        System.out.print("masukkan Baris: ");
        Nbar = in.nextInt();
        System.out.print("masukkan Kolom: ");
        Nkol = in.nextInt();
        int[][] L = new int[Nbar][Nkol];
        int[] L2 = new int[Nbar * Nkol];

        for (i = 0; i < Nbar; i++) {
            for (j = 0; j < Nkol; j++) {
                System.out.print("masukkan Array[" + i + "][" + j + "} : ");
                L[i][j] = in.nextInt();
            }
        }

        System.out.println("\nArray Awal");
        for (i = 0; i < Nbar; i++) {
            for (j = 0; j < Nkol; j++) {
                System.out.print(L[i][j] + "  ");
            }
            System.out.println("");
        }

        app.getSortArray2Dimention(L, L2, Nbar, Nkol);
        System.out.println("\nhasil Array");
        for (i = 0; i < Nbar; i++) {
            for (j = 0; j < Nkol; j++) {
                System.out.print(L[i][j] + "  ");
            }
            System.out.println("");
        }
    }
}