/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class PencarianElemenTerakhir {
    public int getElemenTerakhir(int L[], int n, int X){
        int i= n-1;
        boolean ketemu;
        ketemu = false;
        while((i > -1) && (i <= n-1) && (!ketemu)){
            if (i <= n-1){
                System.out.println("posisi ke-" + i + " isinya " + L[i]);                
            }
            if (L[i] == X){
                ketemu = true;
            }else {
                i = i - 1;
            }
        }
        if (ketemu){ 
            return i;
        }else{
            return -1;
    }
    }
    public static void main(String[] args) {
        int L[] = {13, 16, 14, 21, 76, 15};
        int n = 6;
        
        PencarianElemenTerakhir app = new PencarianElemenTerakhir();
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan X = ");
        int X = in.nextInt();
        System.out.println("indeks = " + app.getElemenTerakhir(L, n, X));
    }
}

