/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class SequentialSearch {
    public boolean getSecSearchBoolean(int L[], int n, int X){
        int i=0;
        while((i < n-1) && (L[i] != X)){
            if (i == 0){
                System.out.println("posisi ke-" + i + "isinya" + L[i]);                
            }
            i = i + 1;
            System.out.println("posisi ke-" + i + " isinya " + L[i]);
        }
        
        if (L[i] == X){ 
            return true;
        }else{
            return false;
        }
    }
    
    public int getSearchOutBoolean(int L[], int n, int X){
        int i = 0;
        while ((i < n-1) && (L[i] != X)){
            i = i + 1;
        }
        if (L[i] == X){
            return i;
        }else {
            return -1;
        }
    }
    public Boolean getSearchInBoolean(int L[], int n, int X){
        int i;
        boolean ketemu;
        
        i = 0;
        ketemu = false;
        while ((i < n) && (!ketemu)){
            if (L[i] == X){
                ketemu = true;
            }else {
                i = i + 1;
            }
        }
        return ketemu;
    }
    public int getSearchInindeks(int L[], int n, int X){
        int i;
        boolean ketemu;
        i = 0;
        ketemu = false;
        
        while ((i < n) && (!ketemu)){
            if (L[i] == X){
                ketemu = true;
            }else {
                i = i + 1;
            }
        }
        if (ketemu){
            return i;
        }else { 
            return -1;
        }
    }
    public int getSearchSentinel(int L[], int n, int X){
        int i, index;
        L[n] = X;
        i = 1;
        while (L[i] != X){
            i = i+1;
        }
        if (i == n){
            index = -1;
        }else {
            index = i;
        }
        return index;
    }
    public static void main(String[] args) {
        //int[] L = {13,16,14,21,76,15};
        int L [] = new int [7];
        L [0] = 13;
        L [1] = 16;
        L [2] = 14;
        L [3] = 21;
        L [4] = 76;
        L [5] = 15;
        int X, n = 6;
        
        SequentialSearch app = new SequentialSearch();
        Scanner in = new Scanner(System.in);
        System.out.print("Masukkan X = ");
        X = in.nextInt();
        
        //System.out.println("index = " +app.getSecSearchBoolean(L, n, X));  
        //System.out.println("index = " +app.getSearchOutBoolean(L, n, X));
        //System.out.println("index = " +app.getSearchInBoolean(L, n, X));
        //System.out.println("index = " +app.getSearchInindeks(L, n, X));
        System.out.println("index = " +app.getSearchSentinel(L, n, X));
    }
}