/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kelima;

/**
 *
 * @author hp
 */
import java.util.Scanner;

public class MenuPersegiPanjangFor {
    public static void main(String[] args) {
        int noMenu = 0;
        int luas, keliling, diagonal, lebar, panjang;
        boolean stop;
        
        stop = false;
        while ((! stop) && (noMenu != 4)){
            System.out.println("Menu empat persegi panjang");
            System.out.println("1. Hitung Luas ");
            System.out.println("2. Hitung Keliling ");
            System.out.println("3. Hitung Panjang Diagonal ");
            System.out.println("4. Keluar Program ");
            System.out.println("Masukan pilihan anda (1/2/3/4) ? ");
            Scanner in = new Scanner(System.in);
            noMenu = in.nextInt();

            switch (noMenu) {
                case 1:
                    System.out.println("masukan nilai panjang");
                    panjang = in.nextInt();
                    System.out.println("masukan nilai lebar");
                    lebar = in.nextInt();
                    luas = panjang * lebar;
                    System.out.println(luas);
                    break;
                case 2:
                    System.out.println("masukan nilai panjang");
                    panjang = in.nextInt();
                    System.out.println("masukan nilai lebar");
                    lebar = in.nextInt();
                    keliling = 2 * panjang + 2 * lebar;
                    System.out.println(keliling);
                    break;
                case 3:
                    System.out.println("masukan nilai panjang");
                    panjang = in.nextInt();
                    System.out.println("masukan nilai lebar");
                    lebar = in.nextInt();
                    diagonal = panjang * panjang + lebar * lebar;
                    System.out.println(diagonal);
                    break;
                case 4:
                    System.out.println("Keluar Program... Sampai Jumpa");
                    break;
            }
        }
    }
}