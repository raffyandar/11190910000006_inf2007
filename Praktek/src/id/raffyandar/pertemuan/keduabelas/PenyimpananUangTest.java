/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.keduabelas;

/**
 *
 * @author hp
 */
public class PenyimpananUangTest {
    public static void main(String[] args) {
        PenyimpananUang tabungan = new PenyimpananUang(5000, 8.5 / 100);
        System.out.println("Uang Yang Ditabung : 5000");
        System.out.println("Tingkat Bunga Yang Sekarang : 8.5%");
        System.out.println("Total Uang Anda Sekarang : " + tabungan.cekUang());
    }
}
