/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.keduabelas;

/**
 *
 * @author hp
 */
public class Manager extends Pegawai{
    
    private int Tunjangan;
    
    public Manager(String nama, int gaji, int tunjangan){
        super(nama, gaji);
       this.Tunjangan = tunjangan;
    }
    public int InfoGaji(){
        return gaji;
    }
    public int infoTunjangan(){
        return Tunjangan;
    }
}
