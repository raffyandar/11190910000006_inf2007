package id.raffyandar.pertemuan.keduabelas;

/**
 *
 * @author hp
 */
public class Programmer extends Pegawai {
    private int bonus;
    private int Tunjangan;
    
    public Programmer(String nama, int gaji, int tunjangan){
         super (nama, gaji);
        this.Tunjangan = tunjangan;
    }
    public int infoGaji(){
        return gaji;
    }
    public int infoBonus(){
        return bonus;
    }
}
