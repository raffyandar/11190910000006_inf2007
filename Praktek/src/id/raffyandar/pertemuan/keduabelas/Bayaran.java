/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.keduabelas;

/**
 *
 * @author hp
 */
public class Bayaran {
    public int hitungBayaran(Pegawai pegawai){
        int uang = pegawai.infogaji();
        if (pegawai instanceof Manager){
            uang += ((Manager) pegawai).infoTunjangan();
        }else if (pegawai instanceof Programmer){
            uang += ((Programmer)pegawai).infoBonus();
        }
     return uang;   
    }
    public static void main(String[] args) {
        Manager m = new Manager("Budi", 800, 50);
        Programmer p = new Programmer("cecep", 600, 30);
        Bayaran upah = new Bayaran();
        System.out.println("Upah Manajer: " + upah.hitungBayaran(m));
        System.out.println("Upah Programmer : " + upah.hitungBayaran(p));
    }
}
