/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.keempat;

/**
 *
 * @author hp
 */
import java.util.Scanner;

public class IndeksNIlaiUjian {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int nilai = in.nextInt();
        char indeks;

        if (nilai >= 80) {
            indeks = 'a';
        } else if ((nilai >= 70) && (nilai < 80)) {
            indeks = 'b';
        } else if ((nilai >= 55) && (nilai < 70)) {
            indeks = 'c';
        } else if ((nilai >= 40) && (nilai < 55)) {
            indeks = 'd';
        } else {
            indeks = 'e';
        }
        System.out.println("indeks = " + indeks);

    }
}
