/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.keempat;

/**
 *
 * @author hp
 */
import java.util.Scanner;

public class GolonganUpahKaryawan {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int jamNormal = 48;
        int upahLembur = 3000;
        String nama = in.next();
        char golongan = in.next().charAt(0);
        int jjk = in.nextInt();
        int jamLembur, upahPerJam = 0, upahTotal;

        if (golongan == 'a') {
            upahPerJam = 4000;
        } else if (golongan == 'b') {
            upahPerJam = 5000;
        } else if (golongan == 'c') {
            upahPerJam = 6000;
        } else if (golongan == 'd') {
            upahPerJam = 7500;
        }
        if (jjk <= jamNormal) {
            upahTotal = (jjk * upahPerJam);
        } else {
            jamLembur = jjk - jamNormal;
            upahTotal = jamNormal * upahPerJam + jamLembur * upahLembur;
        }
        System.out.println(nama + " " + upahTotal);

    }
}
