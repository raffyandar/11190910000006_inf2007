/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kesebelas;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author hp
 */
public class RandomArsip {
    public void setTulis(String file, int position, String record){
        try {
            RandomAccessFile raf = new RandomAccessFile(file, "rw");
            // move file pointer to position specified
            raf.seek(position);
            //writing String to RandomAccessFile 
            raf.writeUTF(record);
            raf.close();
        }catch (IOException e){
            System.err.println("Error : " + e.getMessage());
        }
    }
    public String getBaca(String file, int position) {
        String record = "";
        try{
            RandomAccessFile raf = new RandomAccessFile(file, "r");
            // move file pointer to position specified
            raf.seek(position);
            //reading string from RandomAccessFile
            record =raf.readUTF();
            raf.close();
        }catch (IOException e){
            System.err.println("Error : " + e.getMessage());
        }
        return record;
    }
    public static void main(String[] args) {
        String Berkas = "C:\\Tugas DDP\\file.txt";
        String data = "NIM : 123 | Nama : Budi";
        
        RandomArsip ra = new RandomArsip();
        ra.setTulis(Berkas, 1, data);
        System.out.println("Tulis Berhasil");
        
        String Output = ra.getBaca(Berkas, 1);
        System.out.println("Baca Berhasil : " + Output);
    }
}

        
    

