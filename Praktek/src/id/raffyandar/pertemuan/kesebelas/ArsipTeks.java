/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 *
 * @author hp
 */
public class ArsipTeks {
    int getHitungKarakter(FileReader T){
        char []c;
        int n;
        
        n=0;
        Scanner line = new Scanner(new BufferedReader(T));
        while (line.hasNext()) {
            c = line.next().toCharArray();
            for (char d : c){
                if (d == 'a'){
                    n = n+1;
                }
            }
        }
        return n;
    }
    public static void main(String[] args) {
        ArsipTeks app = new ArsipTeks();
        int a;
        try {
            a = app.getHitungKarakter(new FileReader("C:\\Tugas DDP\\file.txt"));
            System.out.println(a);
        }catch (FileNotFoundException ex){
            Logger.getLogger(ArsipTeks.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
