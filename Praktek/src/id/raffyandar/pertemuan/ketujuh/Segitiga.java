/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class Segitiga {
    private final double alas, luas;
    private final double tinggi;
    
    public Segitiga(){
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan Alas Segitiga = ");
        alas = in.nextDouble();
        System.out.println("Masukkan Tinggi Segitiga = ");
        tinggi = in.nextDouble();      
        luas = (alas * tinggi)/2;
        System.out.println("Luas Segitiga = " + luas);

    }
}
