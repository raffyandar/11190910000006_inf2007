/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.ketujuh;

/**
 *
 * @author hp
 */
public class AplikasilFungsi {
    public static void main(String[] args) {
        float x;
        Fungsi fungsi = new Fungsi();        
        System.out.println("--------------------");
        System.out.println("    x       f(x)    ");
        System.out.println("--------------------");
        
        x = 10;
        while (x <= 15.0) {
            System.out.println(x +    "      " + fungsi.getHasil(x));
            x = (float) (x + 0.2);
        }
        System.out.println("--------------------");
    }
}
