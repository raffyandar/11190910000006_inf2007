/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author hp
 */
public class AplikasiSegitigaParameter {

    public static void main(String[] args) {

        int i, N;
        double a, t;
        Scanner in = new Scanner(System.in);

        System.out.print("N = ");
        N = in.nextInt();

        for (i = 1; i <= N; i++) {
            System.out.print("Alas = ");
            a = in.nextDouble();
            System.out.print("Tinggi = ");
            t = in.nextDouble();            
            SegitigaParameter segitiga = new SegitigaParameter(a, t);
        }

    }
}
