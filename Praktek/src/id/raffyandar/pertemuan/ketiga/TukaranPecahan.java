package id.raffyandar.pertemuan.ketiga;

/**
 *
 * @author hp
 */
import java.util.Scanner;

public class TukaranPecahan {
    public static void main(String[] args) {
        int JumlahUang, pecahan1, pecahan2, pecahan3, pecahan4, pecahan5, sisa;
        Scanner in = new Scanner (System.in);
        System.out.println("jumlah uang : ");
        JumlahUang = in.nextInt();
        
        pecahan1 = JumlahUang / 1000;
        sisa = JumlahUang % 1000;
        pecahan2 = JumlahUang / 500;
        sisa = JumlahUang % 500;
        pecahan3 = JumlahUang / 100;
        sisa = JumlahUang % 100;
        pecahan4 = JumlahUang / 50;
        sisa = JumlahUang % 50;
        pecahan5 = JumlahUang / 25;
        
        System.out.println("tukaran pecahannya");
        System.out.println("Rp. 1000 = " + pecahan1 + " buah");
        System.out.println("Rp. 500 = " + pecahan2 + " buah");
        System.out.println("Rp. 100 = " + pecahan3 + " buah");
        System.out.println("Rp. 50 = " + pecahan4 + " buah");
        System.out.println("Rp. 25 = " + pecahan5 + " buah");
    }
}
