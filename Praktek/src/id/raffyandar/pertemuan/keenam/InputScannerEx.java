package id.raffyandar.pertemuan.keenam;

/**
 *
 * @author hp
 */
import java.util.Scanner;

public class InputScannerEx {
    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan Bilangan : ");
        bilangan = in.nextInt();
        
        System.out.println("Bilangan : " + bilangan);
    }
}
