/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.TiketKeretaApi.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author hp
 */
public class TiketKeretaApi implements Serializable {
    private static final long serialVersionUID = -6756463875294313469L;
    
    private String nama;
    private int kode;
    private String Jurusan;
    private int jumlahTiket;
    private BigDecimal harga;
    private String waktuKeberangkatan;
    private boolean keluar = false;
    private String formatWaktu;
    private int sesi;
    
    public TiketKeretaApi(){
        
    }
    
    public TiketKeretaApi(String nama, int kode, int jumlahTiket, BigDecimal harga, String formatWaktu,
            String waktuKeberangkatan, int sesi){
        this.nama = nama;
        this.kode = kode;
        this.jumlahTiket = jumlahTiket;
        this.harga = harga;
        this.formatWaktu = formatWaktu;
        this.waktuKeberangkatan = waktuKeberangkatan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getKode() {
        return kode;
    }

    public void setKode(int kode) {
        this.kode = kode;
    }

    public String getJurusan() {
        return Jurusan;
    }

    public void setJurusan(String Jurusan) {
        this.Jurusan = Jurusan;
    }

    public int getJumlahTiket() {
        return jumlahTiket;
    }

    public void setJumlahTiket(int jumlahTiket) {
        this.jumlahTiket = jumlahTiket;
    }

    public BigDecimal getHarga() {
        return harga;
    }

    public void setHarga(BigDecimal harga) {
        this.harga = harga;
    }

    public String getformatWaktu() {
        return formatWaktu;
    }

    public int getSesi() {
        return sesi;
    }

    public void setSesi(int sesi) {
        this.sesi = sesi;
    }

    public void setWaktuPembelian(String formatWaktu) {
        this.formatWaktu = formatWaktu;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }

    public String getWaktuKeberangkatan() {
        return waktuKeberangkatan;
    }

    public void setWaktuKeberangkatan(String waktuKeberangkatan) {
        this.waktuKeberangkatan = waktuKeberangkatan;
    }
    
    
    @Override
    public String toString() {
        return "TiketKeretaApi{" + "nama = " + nama + ", kode" + kode + ", Jumlah Tiket =" + jumlahTiket + ", "
                + "Harga = " + harga + ", jumlah harga = " + harga + ", keluar=" + keluar + '}';
    }
}
