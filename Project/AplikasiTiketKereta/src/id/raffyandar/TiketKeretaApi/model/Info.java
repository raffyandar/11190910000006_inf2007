package id.raffyandar.TiketKeretaApi.model;

/**
 *
 * @author hp
 */
public class Info {
    
    private final String aplikasi = "Aplikasi Tiket Kereta";
    private final String versi = "versi 4.0";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getVersi() {
        return versi;
    }
}
