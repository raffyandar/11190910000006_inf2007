/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.raffyandar.TiketKeretaApi.controller;

import com.google.gson.Gson;
import id.raffyandar.TiketKeretaApi.model.TiketKeretaApi;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author hp
 */
public class TiketKeretaApiController {

    private static final String FILE = "F:\\Aplikasi Tiket Kereta\\AplikasiTiketKereta\\lib\\TiketKeretaApi.json";
    private TiketKeretaApi tiket;
    private final Scanner in;
    private String nama;
    private int jumlahTiket;
    private final LocalDateTime waktuPembelian;
    private String waktuKeberangkatan;
    private BigDecimal harga;
    private String Jurusan;
    private final DateTimeFormatter dateTimeFormat;
    private int kode;
    private String formatWaktu;
    private int Pilihan;
    private int sesi;

    public TiketKeretaApiController() {
        in = new Scanner(System.in);
        waktuPembelian = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    }

    public void setMenuPembelian() {
        formatWaktu = waktuPembelian.format(dateTimeFormat);
        System.out.println("============================");
        System.out.println("\tMenu Pembelian");
        System.out.println("============================");

        System.out.print("Nama Pembeli = ");
        nama = in.next();

        System.out.println("001. Bekasi-Semarang    --> Rp. 75.000");
        System.out.println("002. Bekasi-Jogja       --> Rp. 85.000");
        System.out.println("003. Bekasi-Bandung     --> Rp. 70.000");
        System.out.print("Masukkan Kode = ");
        kode = in.nextInt();
        if (kode == 001) {
            Jurusan = "Bekasi-Semarang";
        } else if (kode == 002) {
            Jurusan = "Bekasi-Jogja";
        } else if (kode == 003) {
            Jurusan = "Bekasi-Bandung";
        }
        System.out.println("Jurusan = " + Jurusan);

        String formatWaktuPembelian = waktuPembelian.format(dateTimeFormat);
        System.out.println("Waktu Pembelian : " + formatWaktuPembelian);

        System.out.print("Masukkan Jumlah Tiket = ");
        jumlahTiket = in.nextInt();
        if (jumlahTiket > 0) {
            harga = new BigDecimal(jumlahTiket);
            if (kode == 001) {
                harga = harga.multiply(new BigDecimal(75000));
            } else if (kode == 002) {
                harga = harga.multiply(new BigDecimal(85000));
            } else if (kode == 003) {
                harga = harga.multiply(new BigDecimal(70000));
            }
        }
        System.out.println("Harga = Rp. " + harga);
        System.out.print("Waktu Keberangkatan = ");
        waktuKeberangkatan = in.next();
        System.out.println("Sesi 1 --> Pukul 07.30");
        System.out.println("Sesi 2 --> pukul 10.00");
        System.out.println("Sesi 3 --> Pukul 13.00");
        System.out.print("Sesi = ");
        sesi = in.nextInt();
        switch (sesi) {
            case 1:
                System.out.println("Sesi 1 --> Pukul 07.30");
                break;
            case 2:
                System.out.println("Sesi 2 --> Pukul 10.00");
                break;
            case 3:
                System.out.println("Sesi 3 --> Pukul 13.00");
                break;
            default:
                System.out.println("Tidak Ada Sesi");
                break;

        }
        tiket = new TiketKeretaApi();
        tiket.setNama(nama.toUpperCase());
        tiket.setKode(kode);
        tiket.setJurusan(Jurusan);
        tiket.setJumlahTiket(jumlahTiket);
        tiket.setHarga(harga);
        tiket.setWaktuPembelian(formatWaktu);
        tiket.setSesi(sesi);
        tiket.setWaktuKeberangkatan(waktuKeberangkatan);

        setWriteTiketKeretaApi(FILE, tiket);

        System.out.println("Input Kembali ? ");
        System.out.println("1) Ya  2) Tidak");
        Pilihan = in.nextInt();
        switch (Pilihan) {
            case 1:
                setMenuPembelian();
            case 2:
                Menu app = new Menu();
                app.getMenuAwal();
            default:
                System.out.println("Wrong Keyword");
        }
    }

    public void setWriteTiketKeretaApi(String file, TiketKeretaApi tiket) {
        Gson gson = new Gson();

        List<TiketKeretaApi> tikets = getReadTiketKeretaApi(file);
        tikets.remove(tiket);
        tikets.add(tiket);

        String json = gson.toJson(tikets);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(TiketKeretaApiController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//        public TiketKeretaApi getSearch(String nama) {
//        List<TiketKeretaApi> tiket = getReadTiketKeretaApi(FILE);
//
//        TiketKeretaApi app = tiket.stream()
//                .filter(pp -> nama.equalsIgnoreCase(pp.getNama()))
//                .findAny()
//                .orElse(null);
//
//        return app;
//    }

    public List<TiketKeretaApi> getReadTiketKeretaApi(String file) {
        List<TiketKeretaApi> t = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                TiketKeretaApi[] ps = gson.fromJson(line, TiketKeretaApi[].class);
                t.addAll(Arrays.asList(ps));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TiketKeretaApiController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TiketKeretaApiController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return t;
    }

    public void getInfo() {

        List<TiketKeretaApi> tikets = getReadTiketKeretaApi(FILE);
//        Predicate<Parkir> isKeluar = e -> e.isKeluar() == true;
//        Predicate<Parkir> isDate = e -> e.getWaktuKeluar().toLocalDate().equals(LocalDate.now());

        List<TiketKeretaApi> tResults = tikets.stream().collect(Collectors.toList());
        BigDecimal total = tResults.stream()
                .map(TiketKeretaApi::getHarga)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
//        TiketKeretaApi p = getSearch(nama);
//        TiketKeretaApi ap = new TiketKeretaApi();
        System.out.println("===========================");
        System.out.println("\tINFO TIKET");
        System.out.println("===========================");
        System.out.println("----\t\t----\t\t-------\t\t\t-----\t\t-------------------\t\t----\t\t---------------");
        System.out.println("Nama\t\tKode\t\tJurusan\t\t\tHarga\t\tWaktu Keberangkatan\t\tSesi\t\tWaktu Pembelian");
        System.out.println("----\t\t----\t\t-------\t\t\t-----\t\t-------------------\t\t----\t\t--------------");
        tResults.forEach((t) -> {
            System.out.println(t.getNama() + "\t\t" + t.getKode() + "\t\t" + t.getJurusan() + "\t\t" + t.getHarga() + "\t\t"
                    + t.getWaktuKeberangkatan() + "\t\t\t" + t.getSesi() + "\t\t" + t.getformatWaktu());
        });
        System.out.println("---------------------------------------");
        System.out.println("Total Pendapatan = Rp. " + total + ",-");
        System.out.println("---------------------------------------");

        while (Pilihan != 3) {
            System.out.println("Input Kembali ? ");
            System.out.println("1) Ya  2) Tidak  3) Keluar");
            Pilihan = in.nextInt();
            switch (Pilihan) {
                case 1:
                    setMenuPembelian();
                case 2:
                    Menu app = new Menu();
                    app.getMenuAwal();
                case 3:
                    System.out.println("Program Ditutup, Terimakasih :)");
                    System.exit(0);
                default:
                    System.out.println("Wrong Keyword");
            }
        }
    }
}
